#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
#
#
#
#
require(fs725)
require(cntpstats)
require(fs725sync)
# -----------------------------------------------------------------------------
# Utgard-Lab - enrivonment params
# -----------------------------------------------------------------------------

## Unique IOC instance name
# Used for IOC stats and autosave status

epicsEnvSet("IOCNAME", "LABS-VIP:time-fs725-01")
epicsEnvSet("P", "LABS-VIP:time-fs725-01:")
epicsEnvSet("IPADDR", "172.30.242.31")
epicsEnvSet("PORT", "4001")
epicsEnvSet("EVG", "Utg-VIP:ics-EVG-01:")
epicsEnvSet("EVR", "LabS-Utgard-VIP:TS-EVR-2:")

iocshLoad("$(fs725_DIR)/fs725.iocsh", "P=$(P), IPADDR=$(IPADDR), PORT=$(PORT)")
iocshLoad("$(fs725sync_DIR)/fs725sync.iocsh","P=$(P),  EVR=$(EVR), EVG=$(EVG)")

iocshLoad("$(essioc_DIR)/essioc.iocsh")

dbLoadRecords("sources.db","SYS=,DEV=${DEV}")
dbLoadRecords("tracking.db","SYS=,DEV=${DEV}")

iocInit()

#EOF
